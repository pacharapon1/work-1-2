// PACHARAPON SAENGNGAM 0010
void main() {
  var values = [1, 3, 5, 7, 9];
  var sum = 0;
  for (var value in values) {
    sum += value;
  }
  print(sum); // ผลลัพธ์คือ 25

  // 5 Map – Pizza Ordering
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };

  const order = ['margherita', 'pepperoni', 'pineapple'];

  double totalCost = 0.0;

  for (var pizza in order) {
    if (pizzaPrices.containsKey(pizza)) {
      var price = pizzaPrices[pizza];
      if (price != null) {
        totalCost += price;
      } else {
        print('Price for $pizza is null');
      }
    } else {
      print('$pizza pizza is not on the menu');
    }
  }

  print('Total cost of the order: \$${totalCost.toStringAsFixed(2)}'); // ผลลัพธ์คือ Total cost of the order: $19.00

  //6 Function – Pizza Ordering with Function
  void main() {
    const pizzaPrices = {
      'margherita': 5.5,
      'pepperoni': 7.5,
      'vegetarian': 6.5,
    };
    const order = ['margherita', 'pepperoni', 'pineapple'];

    double calculateTotalCost(List<String> order, Map<String, double> prices) {
      var total = 0.0;
      for (var item in order) {
        final price = prices[item];
        if (price != null) {
          total += price;
        }
      }
      return total;
    }

    var totalCost = calculateTotalCost(order, pizzaPrices);

    print('Total: \$${totalCost.toStringAsFixed(2)}');
  }

}
