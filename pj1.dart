// PACHARAPON SAENGNGAM 0010
import 'dart:io';

void main() {
  //1 Variables
  double temperature = 20;
  int value = 2;
  String pizza = 'pizza';
  String pasta = 'pasta';

  print('I like $pizza and $pasta');
  print('The temperature is ${temperature}C');
  print('$value plus $value makes ${value + value}');

  //2 Temperature Conversion
  double tempFahrenheit = 86;
  double tempCelsius = (tempFahrenheit - 32) / 1.8;
  print('${tempFahrenheit}F = ${tempCelsius.toStringAsFixed(0)}C');

  //3 IF-ELSE Condition - Salary
  print('Enter your net salary:');
  double netSalary = double.parse(stdin.readLineSync()!);

  print('Enter your expenses:');
  double expenses = double.parse(stdin.readLineSync()!);

  if (netSalary > expenses) {
    print('You have saved \$${(netSalary - expenses).toStringAsFixed(2)} this month.');
  } else if (expenses > netSalary) {
    print('You have lost \$${(expenses - netSalary).toStringAsFixed(2)} this month.');
  } else {
    print("Your balance hasn't changed.");
  }
}
